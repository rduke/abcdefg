//
//  AppDelegate.h
//  abcdefg
//
//  Created by Dahiri Farid on 12/11/13.
//  Copyright (c) 2013 Dahiri Farid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
