//
//  HypothesisAnalyser.m
//  abcdefg
//
//  Created by Dahiri Farid on 12/11/13.
//  Copyright (c) 2013 Dahiri Farid. All rights reserved.
//

#import "HypothesisAnalyser.h"

static NSString *const kHypothesis  = @"Hypothesis";
static NSString *const kScore       = @"Score";

@implementation HypothesisAnalyser


//==============================================================================


+ (NSString *)analyseHypothesisArray:(NSArray *)hypothesisArray
{
    NSParameterAssert(hypothesisArray.count > 0);
    
    NSInteger averageScore = 0;
    
    NSMutableArray *notRejectedWords    = NSMutableArray.array;
    NSMutableArray *rejectedWords       = NSMutableArray.array;
    
    for (NSDictionary *hypothesis in hypothesisArray)
    {
        NSString* hypothesisText = hypothesis[kHypothesis];
        
        BOOL isRejectedHypothesis = [hypothesisText rangeOfString:@"REJ"].location != NSNotFound;
        if (isRejectedHypothesis)
        {
            BOOL isFirstHypothesisRejected = [hypothesisArray indexOfObject:hypothesis] == 0;
            if (isFirstHypothesisRejected)
                return nil;
            else
                [rejectedWords addObject:hypothesisText];
        }
        else
            [notRejectedWords addObject:hypothesisText];
        
        averageScore -= [hypothesis[kScore] integerValue];
    }
    
    averageScore = averageScore / hypothesisArray.count;
    
    NSLog(@"Average Score %d", averageScore);
    NSLog(@"Rejected count %d", rejectedWords.count);
    
    NSString *moreProbableWord = notRejectedWords[0];
    
    NSLog(@"moreProbableWord : %@", moreProbableWord);
    
    if (notRejectedWords.count >= 1)
    {
        BOOL shouldRejectBecauseOfAmbiguity     = NO;
        
        for (NSInteger i = 1; i < notRejectedWords.count; i++)
            shouldRejectBecauseOfAmbiguity = ![moreProbableWord isEqualToString:notRejectedWords[i]];
        
        // Probably always YES
        NSLog(@"shouldRejectBecauseOfAmbiguity : %@", shouldRejectBecauseOfAmbiguity ? @"YES" : @"NO");
        
        BOOL shouldRejectBecauseOfStatistics                = NO;
        NSInteger probablyWordMatchCountInRejectedWords     = 0;
        
        for (NSString *rejectectedText in rejectedWords)
        {
            NSRange rangePattern1 = [rejectectedText rangeOfString:moreProbableWord];
            
            if (rangePattern1.location != NSNotFound)
            {
                NSLog(@"Matched in %@", rejectectedText);
                probablyWordMatchCountInRejectedWords++;
            }
        }
        
        if (rejectedWords.count)
            shouldRejectBecauseOfStatistics = probablyWordMatchCountInRejectedWords == 0;
        
        NSLog(@"shouldRejectBecauseOfStatistics : %@", shouldRejectBecauseOfStatistics ? @"YES" : @"NO");
        
//        if (shouldRejectBecauseOfStatistics && shouldRejectBecauseOfAmbiguity)
//            return nil;
    }
    else
        return nil;
    
    return moreProbableWord;
}


//==============================================================================


@end
