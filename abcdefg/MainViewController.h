//
//  MainViewController.h
//  abcdefg
//
//  Created by Dahiri Farid on 12/11/13.
//  Copyright (c) 2013 Dahiri Farid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OpenEars/OpenEarsEventsObserver.h>
#import <Slt/Slt.h>

@class PocketsphinxController;
@class FliteController;

@interface MainViewController : UIViewController<OpenEarsEventsObserverDelegate>
{
    Slt *slt;
    
	// These three are important OpenEars classes that ViewController demonstrates the use of. There is a fourth important class (LanguageModelGenerator) demonstrated
	// inside the ViewController implementation in the method viewDidLoad.
	
	OpenEarsEventsObserver *openEarsEventsObserver; // A class whose delegate methods which will allow us to stay informed of changes in the Flite and Pocketsphinx statuses.
	PocketsphinxController *pocketsphinxController; // The controller for Pocketsphinx (voice recognition).
	FliteController *fliteController; // The controller for Flite (speech).
    
    BOOL usingStartLanguageModel;
	
	// Strings which aren't required for OpenEars but which will help us show off the dynamic language features in this sample app.
	NSString *pathToGrammarToStartAppWith;
	NSString *pathToDictionaryToStartAppWith;
	
	NSString *pathToDynamicallyGeneratedGrammar;
	NSString *pathToDynamicallyGeneratedDictionary;
    
	
	// Our NSTimer that will help us read and display the input and output levels without locking the UI
	NSTimer *uiUpdateTimer;
}

// Example for reading out the input audio levels without locking the UI using an NSTimer

- (void) startDisplayingLevels;
- (void) stopDisplayingLevels;

// These three are the important OpenEars objects that this class demonstrates the use of.
@property (nonatomic, strong) Slt *slt;

@property (nonatomic, strong) OpenEarsEventsObserver *openEarsEventsObserver;
@property (nonatomic, strong) PocketsphinxController *pocketsphinxController;
@property (nonatomic, strong) FliteController *fliteController;


@property (nonatomic, assign) BOOL usingStartLanguageModel;

// Things which help us show off the dynamic language features.
@property (nonatomic, copy) NSString *pathToGrammarToStartAppWith;
@property (nonatomic, copy) NSString *pathToDictionaryToStartAppWith;
@property (nonatomic, copy) NSString *pathToDynamicallyGeneratedGrammar;
@property (nonatomic, copy) NSString *pathToDynamicallyGeneratedDictionary;

// Our NSTimer that will help us read and display the input and output levels without locking the UI
@property (nonatomic, strong) 	NSTimer *uiUpdateTimer;

// UI
@property (nonatomic, strong) IBOutlet UILabel  *lStatus;
@property (nonatomic, strong) IBOutlet UILabel  *lHeard;
@property (nonatomic, strong) IBOutlet UILabel  *lPocketSphinxInputLevel;
@property (nonatomic, strong) IBOutlet UILabel  *lFliteOutputLevel;

@property (nonatomic, strong) IBOutlet UIButton *bStartRecognition;
@property (nonatomic, strong) IBOutlet UIButton *bResumeRecognition;
@property (nonatomic, strong) IBOutlet UIButton *bStopRecognition;
@property (nonatomic ,strong) IBOutlet UIButton *bSuspendRecognition;

- (IBAction)startRecognitionPressed:(id)sender;
- (IBAction)resumeRecognitionPressed:(id)sender;
- (IBAction)stopRecognitionPressed:(id)sender;
- (IBAction)suspendRecognitionPressed:(id)sender;

@end
