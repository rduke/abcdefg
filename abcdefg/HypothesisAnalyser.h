//
//  HypothesisAnalyser.h
//  abcdefg
//
//  Created by Dahiri Farid on 12/11/13.
//  Copyright (c) 2013 Dahiri Farid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HypothesisAnalyser : NSObject

+ (NSString *)analyseHypothesisArray:(NSArray *)hypothesisArray;

@end
